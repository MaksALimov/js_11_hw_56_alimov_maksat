import React, {useState} from "react";
import {nanoid} from "nanoid";
import meatImg from './assets/meat.svg';
import cheeseImg from './assets/cheese.svg';
import saladImg from './assets/salad.svg';
import baconIMg from './assets/bacon.svg';
import AddIngredients from "./components/AddIngredients/AddIngredients";
import BurgerIngredients from "./components/BurgerIngredients/BurgerIngredients";
import BurgerPrice from "./components/BurgerPrice/BurgerPrice";
import './components/AddIngredients/AddIngredients.css'


const App = () => {
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', price: 50, count: 0, id: nanoid(), image: meatImg},
        {name: 'Cheese', price: 20, count: 0, id: nanoid(), image: cheeseImg},
        {name: 'Salad', price: 5, count: 0, id: nanoid(), image: saladImg},
        {name: 'Bacon', price: 30, count: 0, id: nanoid(), image: baconIMg},
    ]);

    const [supplements, setSupplements] = useState([])

    const addIngredients = (id, name) => {
        setIngredients(ingredients.map(ingredients => {
            if (ingredients.id === id) {
                supplements.push({name, id})
                return {...ingredients, count: ingredients.count + 1}
            }
            return ingredients;
        }));
    };

    const removeIngredients = (id) => {
        setIngredients(ingredients.map(ingredients => {
            if (ingredients.id === id) {
                const additive = [...supplements];
                const index = additive.findIndex(el => el.id === id);
                additive.splice(index, 1);
                setSupplements(additive);
                if (ingredients.count <= 0) {
                    return {...ingredients, count: 0}
                }

                return {...ingredients, count: ingredients.count - 1};
            }

            return ingredients;
        }));
    };

    return (
        <div>
            <div className="wrapper">
                <div>
                    <fieldset>
                        <legend className="ingredients-title">Ingredients</legend>
                        {ingredients.map(ingredients => {
                            return <AddIngredients
                                key={ingredients.id}
                                ingredients={ingredients.name}
                                counter={() => addIngredients(ingredients.id, ingredients.name)}
                                remove={() => removeIngredients(ingredients.id)}
                                count={ingredients.count}
                                price={ingredients.price}
                                img={ingredients.image}
                            />
                        })}
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <legend className="ingredients-title">Burger</legend>
                        <div className="Burger">
                            <div className="BreadTop">
                                <div className="Seeds1"/>
                                <div className="Seeds2"/>
                            </div>
                            <BurgerIngredients supplements={supplements}/>
                            <div className="BreadBottom"/>
                        </div>
                        <BurgerPrice ingredients={ingredients}/>
                    </fieldset>
                </div>
            </div>
        </div>
    )
};

export default App;
