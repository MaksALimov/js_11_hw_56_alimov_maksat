import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import Counter from "../Counter/Counter";

const AddIngredients = ({ingredients, counter, remove, count, price, img}) => {
    return (
        <div className="wrapper">
            <div className="buttons-wrapper">
                <span className="neon">{price} сом</span>
                <button onClick={counter} className="glow-on-hover ingredients">
                    {ingredients}
                    <img src={img} alt="ingredients"/>
                </button>
                <Counter count={count}/>
                <button disabled={count === 0 ? [true] : false} onClick={remove} className="TrashButton">
                    <FontAwesomeIcon icon={faTrashAlt}/>
                </button>
            </div>
        </div>
    );
};

export default AddIngredients;