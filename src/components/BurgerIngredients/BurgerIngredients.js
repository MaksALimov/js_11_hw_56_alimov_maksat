import React from 'react';

const BurgerIngredients = ({supplements}) => {
    return (
        <>
            {supplements.map((ingredients, i) => {
                return <div key={i} className={ingredients.name}/>
            })}
        </>
    );
};

export default BurgerIngredients;