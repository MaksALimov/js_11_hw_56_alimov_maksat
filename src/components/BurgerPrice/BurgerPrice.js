import React from 'react';
import './BurgerPrice.css'

const BurgerPrice = ({ingredients}) => {
    return (
        <>
            <p className="neon">Price:
                {ingredients.reduce((total, amount) => {
                    return total + amount.count * amount.price
                }, 20)}
            </p>
        </>
    );
};

export default BurgerPrice;