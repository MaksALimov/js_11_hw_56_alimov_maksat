import React from 'react';
import './Counter.css'

const Counter = ({count}) => {
    return (
        <div>
            <p className="neon">Counter x{count}</p>
        </div>
    );
};

export default Counter;