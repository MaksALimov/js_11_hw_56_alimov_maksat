import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import './TrashButton.css'


const TrashButton = ({count, remove,}) => {
    return (
        <div>
            <button className="trash" disabled={count === 0 ? [true]: false} onClick={remove}><FontAwesomeIcon icon={faTrashAlt}/></button>
        </div>
    );
};

export default TrashButton;